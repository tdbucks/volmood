# Background
volMood started as an idea to capture what people were feeling during football games, but will eventually evolve into something that anyone can use as a way to guage the general mood of any event.

# Usage
### Installing NodeJS
NodeJS is open source and cross-platform, so just about any OS can utilize most of its tools. To install NodeJs on your machine, visit https://nodejs.org/en/download/ .

To test that Node is working correctly, run `node -v` to see which version you're currently working with.

### Using the script
With NodeJS installed, simply pull the repository down and run `node index.js` on your local machine. The current RSS feed is set to #vols, but any twitter RSS feed will work.
